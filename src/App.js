import { useEffect, useState } from "react";
import "./App.css";
import Cart from "./component/Cart/Cart";
import Product from "./component/Products/Product";

function App() {
  const [products, setProducts] = useState([]);
  let [cart, setCart] = useState([]);
  let [selectedCart, setSlected] = useState([]);

  // load data from json
  useEffect(() => {
    fetch("gun.json")
      .then((res) => res.json())
      .then((data) => setProducts(data));
  }, []);

  // set cart handler
  const addToCart = (product) => {
    if (cart.indexOf(product) === -1) {
      const newCart = [...cart, product];
      setCart(newCart);
    }
  };

  // chose again
  const choseAgain = () => {
    setCart((cart = []));
    setSlected((selectedCart = []));
  };
  // chose for me
  const choseForMeHandler = () => {
    let itemWillBe = cart[Math.floor(Math.random() * cart.length)];
    setSlected(itemWillBe);
  };
  return (
    <div className="App">
      <div className="product_header">
        <h3>Gun buyers House</h3>
        <p>chose your own</p>
      </div>
      <div className="section_wrapper">
        <div className="product_wrapper">
          {products.map((product) => (
            <Product
              key={product.id}
              product={product}
              addToCart={addToCart}
            ></Product>
          ))}
        </div>
        <div className="cart_boxes">
          <h2 className="cart_title">selected in cart </h2>
          {cart.map((item) => (
            <Cart key={item?.id} item={item} />
          ))}
          <h4>
            {selectedCart?.name ? `selected : ` + selectedCart?.name : ""}
          </h4>
          <div className="chose_btns">
            <button
              onClick={choseForMeHandler}
              className="home9_primary_btn add_to_cart"
            >
              Chose for Me
            </button>
            <button
              onClick={choseAgain}
              className="home9_primary_btn add_to_cart"
            >
              Chose Again
            </button>
          </div>
        </div>
      </div>
      <div className="question_box_wrapper">
        <div className="question_box">
          <h2>Props vs State:</h2>
          <p>
            Props হচ্ছে react এর অসাধারণ একটি ফিচার | এটার মাদ্ধমে আমরা ডাটা
            প্যারেন্ট থেকে চাইল্ড কম্পোনেন্ট এ পাঠাতে পারি | প্রপ্স এর value
            কখনো চেঞ্জ করা যায় না। state এর মাদ্ধমে আমরা আমরা data এর একটি মান
            সেট করতে পারি| প্রয়োজন অনুযায়ী পরিবর্তন ও করতে পারি | বলা যায় props
            অপরিবর্তনশীল আর state পরিবর্তনশীল
          </p>
        </div>
        <div className="question_box">
          <h2>useState কিভাবে কাজ করে :</h2>
          <p>
            let [cart, setCart] = useState([])| এটা useState ব্যবহার করার জন্যে
            লিখা হয়। useState([]) এটার মাদ্ধমে ইনিশিয়াল ভাবে একটা ভ্যালু সেট করা
            হয়। ইনিশিয়াল ভ্যালু কি সেট করা হবে তা নির্ভর করে আমরা কোন ডাটা নিয়ে
            কাজ করবো তার উপর। setCart এটকি ফাঙ্কশন যা ব্যবহার করে আমরা cart এর
            ভ্যালু কে আপডেট করি। এটা ReactJs এর একটি hook
          </p>
        </div>
      </div>
    </div>
  );
}

export default App;
