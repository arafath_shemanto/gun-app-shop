import React from "react";
import "./Product.css";
import { FaShoppingCart } from "react-icons/fa";
import { AiFillStar } from "react-icons/ai";
const Product = ({ product, addToCart }) => {
  const { img, name, id, price } = product;
  return (
    <div className="product_widget10">
      <div className="product_thumb_upper">
        <a href="#" className="thumb">
          <img src={img} alt="" />
        </a>
      </div>
      <div className="offer_badge_box">
        <div className="offer_badge">20%</div>
        <div className="offer_badge New_badge">New</div>
      </div>

      <div className="product__meta">
        <div className="brand_rating d-flex align-items-center justify-content-between">
          <span className="product_banding ">ID : {id}</span>
          <div className="stars justify-content-start">
            <i>
              <AiFillStar />
            </i>
            <i>
              <AiFillStar />
            </i>
            <i>
              <AiFillStar />
            </i>
            <i>
              <AiFillStar />
            </i>
            <i>
              <AiFillStar />
            </i>
          </div>
        </div>

        <a href="product_details.php">
          <h4>{name}</h4>
        </a>

        <div className="product_prise">
          <p>${price}</p>
          <button
            onClick={() => addToCart(product)}
            className="home9_primary_btn add_to_cart"
          >
            Add to cart <FaShoppingCart />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Product;
