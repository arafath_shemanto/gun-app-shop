import React from "react";
import "./Cart.css";
const Cart = ({ item }) => {
  const { img, name } = item;
  return (
    <div className="single_cart">
      <div className="thumb">
        <img src={img} alt="" />
      </div>
      <div className="product_content">
        <h4>{name}</h4>
      </div>
    </div>
  );
};

export default Cart;
